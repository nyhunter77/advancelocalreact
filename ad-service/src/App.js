import React from 'react';
import ReactDOM from 'react-dom';

import AdComponent from './adComponent';

ReactDOM.render(
  <AdComponent />,
  document.querySelector('#root')
);

export default AdComponent;
