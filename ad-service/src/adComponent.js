import React, { Component } from 'react';

// I would like to mention I would have liked to use react hooks here but since a requirement was for a single component, it needed to be a Class component and hooks only work with functional components so I would have had to create a few components to finish the project but I think that would have been a better way to go to further modularize my code.

// Also, wanted to mention I started the project with Create-react-app. 
// Hopefully, that's ok, I do have my own instructions for creating a react app from scratch, it's still a long ish, tedious, process though. Thanks

class Ads extends Component {
  buttonClicked() {
    const googletag = window.googletag || {};
    googletag.cmd = googletag.cmd || [];

    // randomly setting targeting - sets the first via coinflip and then other ad unit gets opposite category of targeting with random deeper value
    const keyValuePairs = [{'interests':['sports','music','movies']},{'sports':['racing','cricket','baseball']}];
    const coinFlip = Math.round(Math.random());
    const fineTarget = Math.floor((Math.random()*3));

    const key = Object.keys(keyValuePairs[coinFlip]).toString();
    const key2 = Object.keys(keyValuePairs[coinFlip === 1 ? 0 : 1]).toString();

    let value;
    let value2;
    if (coinFlip === 1) {
      value = keyValuePairs[coinFlip].sports[fineTarget];
      value2 = keyValuePairs[0].interests[fineTarget];
    } else {
      value = keyValuePairs[coinFlip].interests[fineTarget];
      value2 = keyValuePairs[1].sports[fineTarget];
    }
    // targets set for slot level
    
    googletag.cmd.push(() => {
      googletag.defineSlot('344101295/SI/www.silive.com/news/index.ssf', [728, 90], 'headerAd').addService(googletag.pubads())
        .setTargeting(key, value); // slot level 
      googletag.defineSlot('344101295/SI/www.silive.com/news/index.ssf', [300, 250], 'body1').addService(googletag.pubads())
        .setTargeting(key2, value2); // slot level
      googletag.pubads().setTargeting("topic","basketball"); // page level
      googletag.pubads().enableSingleRequest();
      googletag.enableServices();
      googletag.display('headerAd');
      googletag.display('body1');
    });
  }

  render() {
    return (
      <div id="adContainer">
        <button onClick={this.buttonClicked}>Click Me!</button>
        <div
          id="headerAd"
          style={{ width: '728px', height: '90px' }}
        />
        <div
          id="body1"
          style={{ margin: '0 auto', width: '300px', height: '250px' }}
        />
      </div>
    );
  }
};

export default Ads;